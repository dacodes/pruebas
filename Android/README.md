# Prueba Android

### Descripción
> La siguiente prueba consiste en construir un aplicación en Android. Deberá llamarse Venados Test.
>

### Instrucciones
> Utiliza Git para Documentar tu código realizando commits conforme realizes las tareas.
>
> Al finalizar la prueba deberá contener un archivo README que explique exactamente los pasos necesarios para construir y ejecutar el proyecto.
>


##### Recursos
```
Header
    Accept: application/json

Servicios (GET)
    https://venados.dacodes.mx/api/statistics
    https://venados.dacodes.mx/api/games
    https://venados.dacodes.mx/api/players
    https://venados.dacodes.mx/api/sponsors
    https://venados.dacodes.mx/api/notifications
```

#### Pantalla Principal

> Deberá contener un drawer layout con un header y con los items Home y Estadísticas y Jugadores. Al dar click sobre los items, la aplicación despliega el fragmento correspondiente.
>
> En vez de la imagen de perfil en el header, se desplegará el escudo de Venados F.C.
>
![](https://resources-decodes.s3.amazonaws.com/pantallaprincipal.png)


#### Vista Principal
>Deberá de tener un coordinator layout, la cual contendrá el diseño que se puede observar en la parte superior de la pantalla. Cada tab deberá contener un fragment con un recyclerview para desplegar la lista de partidos de la Copa MX y otro con la lista de partidos de Ascenso MX. La celda para ambas listas deberá ser como se muestra en el diseño a continuación. Al hacer slide hacia arriba, el tablayout deberá trasladarse hacia arriba, ocultando el escudo de Venados F.C.
>
>Las listas deberán de tener Swipe to refresh para poder actualizar el listado.
>
>El endpoint a consumir es el siguiente:

```
GET
https://venados.dacodes.mx/api/games
```

>En la respuesta del GET, se reciben los partidos correspondientes a Venados F.C., los partidos deberán de ser filtrados al torneo correspondiente: COPA MX ->COM y ASCENSO MX ->LDA. Dentro de cada partido podrás obtener los equipos contrarios a Venados F.C. con sus escudos correspondiente así como los datos del partido. La lista deberá de tener un separador por mes como se puede observar en el diseño.
>
>Al dar click al ícono del calendario en la celda, se agendará en el calendario.
>
![](https://resources-decodes.s3.amazonaws.com/fragment1-vistapricipal.png)

#### Estadísticas

>En esta pantalla se observarán las estadísticas de la tabla general. Se desplegarán los datos siguientes: Posición, JJ (Juegos jugados), DG (Diferencia de goles), PTS (Puntos).
>
```
GET
https://venados.dacodes.mx/api/statistics
```
![](https://resources-decodes.s3.amazonaws.com/estadisticas.png)


#### Jugadores

>Se desplegará un gridview desplegando las fotografías de los jugadores. Cada celda del gridview deberá contener la posición desempeñada por el jugador y su nombre. Al darle click a un jugador, se desplegará una ventana encima con los datos detallados del jugador.
```
GET
https://venados.dacodes.mx/api/players
```
![](https://resources-decodes.s3.amazonaws.com/jugadores.png)

#### Detalle de Jugador
> Esta ventana contiene los datos detallados del jugador, sin salir del fragmento de Jugadores. La información se obtiene del mismo endpoint de jugadores.
>
![](https://resources-decodes.s3.amazonaws.com/detallejugador.png)
